import React from 'react';
import logo from './logo.svg';
import './App.css';
import Nav from "./components/Nav.js";
import Landing from "./components/Landing.js"
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Contact from "./components/Contact.js";

function App() {
  return (
  	 <div className="App">
  	 <BrowserRouter>
			<Nav />

			<Route exact path="/" component={Landing} />
			<Route path="/contact" component={Contact} />
			
		</BrowserRouter>

    	
    </div>
  );
}

export default App;
