import React from 'react';
import LeftComponent from "./LeftComponent.js";
import RightComponent from "./RightComponent.js";


function Landing() {
  return (
  	
    	<div className="container"> 
    	

    		<div className="row">
    			<div className="col-12 col-md-6">
    				<LeftComponent />
      			
      			</div>

      			<div className="col-12 col-md-6">
    				<RightComponent />
      			</div>
      		</div>

    	</div>

   
  );
}

export default Landing;
