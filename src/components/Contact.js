import React from "react";

function Contact(){
	return(
		<div className="container mt-5" > 
    	

    		<div className="row">
    			<div className="col-12 col-md-6">
    				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d123504.19388676457!2d120.9921492594823!3d14.683950152531386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397ba0942ef7375%3A0x4a9a32d9fe083d40!2sQuezon%20City%2C%20Metro%20Manila!5e0!3m2!1sen!2sph!4v1574423295184!5m2!1sen!2sph"  
	    				id= "gmap"
	    				frameborder="0" 
	    				style={{border:0}} 
	    				allowfullscreen
    				></iframe>
      			
      			</div>

      			<div className="col-12 col-md-6 text-left">
    				<form>
    				 <div className="form-group">
					    <label for="name">Name:</label>
					    <input type="text" className="form-control" id="name" placeholder="Please enter name" />
					  </div>
					  <div className="form-group">
					    <label for="exampleInputEmail1">Email address</label>
					    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
					    <small id="emailHelp" className="form-text text-muted">We`ll never share your email with anyone else.</small>
					  </div>
					 
					<label for="message">Message:</label>
					<textarea className="form-control" cols="30" rows="10" />
					  <button type="submit" className="btn btn-primary">Submit</button>
					</form>
      			</div>
      		</div>

    	</div>

		)
}

export default Contact;
